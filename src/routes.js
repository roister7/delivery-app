import PickupTasks from './pages/PickupTasks';
import DeliveryTasks from './pages/DeliveryTasks';
import Deliver from './pages/Deliver';
import Map from './pages/Map';
import Feedback from './pages/Feedback';
import Profile from './pages/Profile';
import Notice from './pages/Notice';
import Signin from './pages/Signin';
import Issue from './pages/Issue';
import Payment from './pages/Payment';
import Taskhistory from './pages/Taskhistory';
import NotFoundPage from './pages/not-found.vue';

export default [
  {
    path: '/',
    component: PickupTasks
  },
  {
    path: '/pickuptasks/map/',
    component: Map
  },
  {
    path: '/feedback/',
    component: Feedback
  },
  {
    path: '/signin/',
    component: Signin
  },
  {
    path: '/deliverytasks/',
    component: DeliveryTasks
  },
  {
    path: '/deliverytasks/deliver/',
    component: Deliver
  },
  {
    path: '/deliverytasks/map/',
    component: Map
  },
  {
    path: '/profile/',
    component: Profile
  },
  {
    path: '/issue/',
    component: Issue
  },
  {
    path: '/payment/',
    component: Payment
  },
  {
    path: '/notice/',
    component: Notice
  },
  {
    path: '/taskhistory/',
    component: Taskhistory
  },
  {
    path: '(.*)',
    component: NotFoundPage
  }
];
